# dir over all OpenHome dirs
TOPOH=@TOPOH@/
TOPOHNET=@TOPOHNET@/
TOPOHNETGENERATED=@TOPOHNETGENERATED@/
TOPOHSONGCAST=@TOPOHSONGCAST@/

OTHERLIBS=@OTHERLIBS@

OHBTYPE=Release

# Top ohSongcast dir
TOPSC = $(TOPOH)ohSongcast/
# Internal path to obj inside ohNet, ohSongcast etc.
OBJIPATH = Build/Obj/Posix/$(OHBTYPE)/
# ohSongcast other objects
TOPSCO=$(TOPSC)$(OBJIPATH)


AM_CPPFLAGS = -DDEBUG -g -Wall \
            -DDATADIR=\"${pkgdatadir}\" -DCONFIGDIR=\"${sysconfdir}\" \
            -DPACKAGE_VERSION=\"${PACKAGE_VERSION}\" 

if WITH_OHBUILD
AM_CPPFLAGS += \
            -I$(TOPOH)ohNet/Build/Include/ \
            -I$(TOPOH)ohNetGenerated/Build/Include/ \
            -I$(TOPOH)ohTopology/build/Include/  \
            -I$(TOPSCO)  \
            -I$(TOPSC) \
            -DWITH_OHBUILD
else
AM_CPPFLAGS += -I$(TOPOHNET)include/ohNet
endif

AM_CPPFLAGS += $(libmpdclient_CFLAGS)

if WAVSC2
AM_CPPFLAGS += \
            -I$(top_srcdir)/mpd2src \
            -I$(top_srcdir)/sc2src \
            -DWITH_WAVSC2
endif

AM_CXXFLAGS = -std=c++17

bin_PROGRAMS = sc2mpd mpd2sc

if WITH_OHBUILD
sc2mpd_LDADD = $(TOPSCO)libohSongcast.a \
     $(TOPOH)ohNet/$(OBJIPATH)libohNetCore.a \
     $(TOPOH)ohNet/$(OBJIPATH)libTestFramework.a $(OTHERLIBS)
else
sc2mpd_LDADD = \
     -L${TOPOHSONGCAST}lib/ohSongcast -lohSongcast \
     -L${TOPOHNETGENERATED}lib/ohNetGenerated -lohNetGeneratedDevices \
     -L${TOPOHNET}lib/ohNet -lohNetCore \
     -L${TOPOHNET}lib/ohNet -lTestFramework \
     $(OTHERLIBS)
endif
sc2mpd_CPPFLAGS = -I$(top_srcdir) $(AM_CPPFLAGS) 

sc2mpd_SOURCES = \
     sc2src/alsadirect.cpp \
     sc2src/audiodecoder.cpp \
     sc2src/audiodecoder.h \
     sc2src/chrono.cpp \
     sc2src/chrono.h \
     sc2src/conf_post.h \
     sc2src/conftree.cpp \
     sc2src/conftree.h \
     sc2src/httpgate.cpp \
     sc2src/log.cpp \
     sc2src/log.h \
     sc2src/pathut.cpp \
     sc2src/pathut.h \
     sc2src/pcmdecoder.cpp \
     sc2src/pcmdecoder.h \
     sc2src/rcvqueue.h \
     sc2src/sc2mpd.cpp \
     sc2src/songcastreceiver.cpp \
     sc2src/songcastreceiver.h \
     sc2src/smallut.cpp \
     sc2src/smallut.h \
     sc2src/volwatch.cpp \
     sc2src/volwatch.h \
     sc2src/watcher.cpp \
     sc2src/watcher.h \
     sc2src/wav.cpp \
     sc2src/wav.h \
     sc2src/workqueue.h

if FLAC
sc2mpd_CPPFLAGS += $(FLAC_CFLAGS)
sc2mpd_SOURCES += \
     sc2src/flacdecoder.cpp \
     sc2src/flacdecoder.h
endif

if WAVSC2
sc2mpd_SOURCES += \
     mpd2src/openaudio.cpp \
     mpd2src/wavreader.cpp \
     mpd2src/fiforeader.cpp \
     mpd2src/stringtotokens.cpp
endif

OTHEROMP2 = $(TOPSCO)libohSongcast.a \
             $(TOPOH)ohNetGenerated/$(OBJIPATH)DvAvOpenhomeOrgSender1.o

if WITH_OHBUILD
mpd2sc_LDADD = $(OTHEROMP2) $(TOPOH)ohNet/$(OBJIPATH)libohNetCore.a \
     $(TOPOH)ohNet/$(OBJIPATH)libTestFramework.a $(OTHERLIBS)
else
mpd2sc_LDADD = \
     -L${TOPOHSONGCAST}lib/ohSongcast -lohSongcast \
     -L${TOPOHNETGENERATED}lib/ohNetGenerated -lohNetGeneratedDevices \
     -L${TOPOHNET}lib/ohNet -lohNetCore \
     -L${TOPOHNET}lib/ohNet -lTestFramework \
     $(OTHERLIBS)
endif

mpd2sc_CPPFLAGS = -I$(top_srcdir)/sc2src $(AM_CPPFLAGS)
mpd2sc_SOURCES = \
     mpd2src/audioencoder.cpp \
     mpd2src/audioencoder.h \
     mpd2src/audioreader.h \
     mpd2src/audioutil.h \
     mpd2src/base64.cxx \
     mpd2src/base64.hxx \
     mpd2src/fiforeader.cpp \
     mpd2src/fiforeader.h \
     mpd2src/icon.h \
     mpd2src/mpd2sc.cpp \
     mpd2src/openaudio.cpp \
     mpd2src/openaudio.h \
     mpd2src/pcmencoder.cpp \
     mpd2src/pcmencoder.h \
     mpd2src/songcastsender.cpp \
     mpd2src/songcastsender.h \
     mpd2src/stringtotokens.cpp \
     mpd2src/stringtotokens.h \
     mpd2src/wavreader.cpp \
     mpd2src/wavreader.h \
     sc2src/log.cpp

EXTRA_DIST = \
ohpatches/ohNet/0001-t4-arm.patch \
ohpatches/ohNetGenerated/001-makefile-arm-archs.patch \
ohpatches/ohNetGenerated/002-common-mak-not4.patch 

if FLAC
mpd2sc_CPPFLAGS += $(FLAC_CFLAGS)
mpd2sc_SOURCES += \
     mpd2src/flacencoder.cpp \
     mpd2src/flacencoder.h
endif

dist_bin_SCRIPTS = mpd2src/scmakempdsender
dist_noinst_SCRIPTS = ohbuild.sh

dist-hook:
	test -z "`git status -s | grep -v sc2mpd-$(VERSION)`"
	git tag -a sc2mpd-v$(VERSION) -m 'version $(VERSION)'

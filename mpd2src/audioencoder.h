/* Copyright (C) 2015-2018 J.F.Dockes
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef _AUDIOENCODER_H_INCLUDED_
#define _AUDIOENCODER_H_INCLUDED_

#include <string.h>

#ifdef WITH_OHBUILD
#include "OhmSender.h"
#else
#include <OpenHome/OhmSender.h>
#endif

#include "audioreader.h"

using namespace std;

using namespace OpenHome;
using namespace OpenHome::Av;

class AudioEncoder {
public:
    AudioEncoder(AudioReader *audioReader, OhmSenderDriver *ohmSender,
                 bool lossless, const char* name)
        : m_audioReader(audioReader), m_ohmSender(ohmSender), m_name(name)
    {
        m_ohmSender->SetAudioFormat(m_audioReader->sampleRate(),
                                    m_audioReader->byteRate() * 8,
                                    m_audioReader->numChannels(),
                                    m_audioReader->bitsPerSample(),
                                    lossless,
                                    Brn(name));
    }
    virtual ~AudioEncoder() {}
    virtual void start() = 0;
    virtual int encode(const unsigned char *buffer, unsigned num_bytes, bool halt) = 0;
    virtual void finish() = 0;
    const std::string name() { return m_name; };
protected:
    AudioReader *m_audioReader;
    OhmSenderDriver *m_ohmSender;
    bool m_lossless;
    std::string m_name;
};

class AudioEncoderFactory {
public:
    static AudioEncoder* CreateAudioEncoder(const string& codec,
                                            AudioReader *audioReader,
                                            OhmSenderDriver *ohmSender,
                                            unsigned packetBytes);
};

#endif /* _AUDIOENCODER_H_INCLUDED_ */

/* Copyright (C) 2015-2018 J.F.Dockes
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef _FLACENCODER_H_INCLUDED_
#define _FLACENCODER_H_INCLUDED_

#include <FLAC/stream_encoder.h>

#ifdef WITH_OHBUILD
#include "OhmSender.h"
#else
#include <OpenHome/OhmSender.h>
#endif

#include "audioencoder.h"
#include "audioreader.h"

using namespace OpenHome;
using namespace OpenHome::Av;

class FlacEncoder : public AudioEncoder {
    static const int SAMPLES_BUF_SIZE = 16 * 1024;

public:
    FlacEncoder(AudioReader *audioReader, OhmSenderDriver *ohmSender);
    ~FlacEncoder();

    void start();
    int encode(const unsigned char *buffer, unsigned num_bytes, bool halt);
    void finish();

private:
    static FLAC__StreamEncoderWriteStatus writeCallback(
            const FLAC__StreamEncoder *encoder,
            const FLAC__byte buffer[],
            size_t bytes,
            unsigned samples,
            unsigned current_frame,
            void *client_data);

    FLAC__StreamEncoder*  m_encoder;
    FLAC__int32           m_samplesBuf[SAMPLES_BUF_SIZE];
    bool m_halt;

#ifdef DEBUG_ENCODER
    int dumpfd;
#endif
};

#endif /* _FLACENCODER_H_INCLUDED_ */

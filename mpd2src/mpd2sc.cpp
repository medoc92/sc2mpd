/*
Copyright 2012, OpenHome. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

   1. Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.

THIS SOFTWARE IS PROVIDED BY OPENHOME ''AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation
are those of the authors and should not be interpreted as representing
official policies, either expressed or implied, of OpenHome.
*/

#include <OpenHome/OhNetTypes.h>
#include <OpenHome/Net/Core/DvDevice.h>
#include <OpenHome/Net/Core/OhNet.h>
#include <OpenHome/Net/Core/CpDevice.h>
#include <OpenHome/Net/Core/CpDeviceUpnp.h>
#include <OpenHome/Private/Ascii.h>
#include <OpenHome/Private/Thread.h>
#include <OpenHome/Private/OptionParser.h>
#include <OpenHome/Private/Debug.h>
#include <OpenHome/Os.h>
#include <OpenHome/Private/Env.h>

#include <cstring>
#include <fstream>
#include <iostream>
#include <vector>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/un.h>

#ifdef WITH_OHBUILD
#include "OhmSender.h"
#else
#include <OpenHome/OhmSender.h>
#endif

#include "log.h"
#include "icon.h"
#include "audioreader.h"
#include "openaudio.h"
#include "base64.hxx"
#include "songcastsender.h"

using namespace std;

using namespace OpenHome;
using namespace OpenHome::Net;
using namespace OpenHome::TestFramework;
using namespace OpenHome::Av;

int g_sockfd = -1;

// Sig catcher so that we can interrupt the pause() which will be
// waiting for playing to end. Also set a flag for the benefit of busyRdWr()
bool g_quitrequest = false;
void sigcatcher(int)
{
    LOGDEB("sigcatcher\n");
    g_quitrequest = true;

    // As recvfrom() is a blocking operation we need to shut the corresponding
    // socket down so that recvfrom() returns with 0.
    if (g_sockfd > 0) {
        shutdown(g_sockfd, SHUT_RD);
    }
}

#define SOCK_PATH "/tmp/mpd2sc.sock"
#define BUF_SIZE 16
void HandleUserCmd(OhmSender* sender, SongcastSender* scsender)
{
    struct sockaddr_un server_addr, client_addr;
    socklen_t len;
    ssize_t num_bytes;
    char buf[BUF_SIZE];

    LOGDEB("SongcastSender: Running user command handler\n");

    g_sockfd = socket(AF_UNIX, SOCK_DGRAM, 0);
    if (g_sockfd == -1) {
        LOGERR("Error: Cannot create socket: " << strerror(errno) << endl);
        goto _leave;
    }

    if (remove(SOCK_PATH) == -1 && errno != ENOENT) {
        LOGERR("Error: Cannot remove socket path: " << strerror(errno) << endl);
        goto _close_socket;
    }

    memset(&server_addr, 0, sizeof(struct sockaddr_un));
    server_addr.sun_family = AF_UNIX;
    strncpy(server_addr.sun_path, SOCK_PATH, sizeof(server_addr.sun_path) - 1);

    if (bind(g_sockfd, (struct sockaddr *) &server_addr,
                            sizeof(struct sockaddr_un)) == -1) {
        LOGERR("Error: Cannot bind to socket: " << strerror(errno) << endl);
        goto _close_socket;
    }

    while (!g_quitrequest) {
        len = sizeof(struct sockaddr_un);
        // recvfrom() is a blocking call and will wait for messages to arrive.
        // We are shutting down the socket in the signal catcher in which case
        // recvfrom() shall return 0. As g_quitrequest is set in the signal
        // catcher, the endless loop is quit.
        num_bytes = recvfrom(g_sockfd, buf, BUF_SIZE, 0,
                            (struct sockaddr *) &client_addr, &len);

        if (num_bytes == -1) {
            LOGERR("Error: Failed to receive from socket: " <<
                            strerror(errno) << endl);
            continue; // Ignore failed request
        }
        if (num_bytes > 0) {
            string response("OK");

            LOGDEB("HandleUserCmd: Received " << num_bytes << " bytes: " <<
                            string(buf, num_bytes) << endl);
            if (strncmp("play", buf, num_bytes) == 0)
                scsender->Play();
            else if (strncmp("stop", buf, num_bytes) == 0)
                scsender->Stop();
            else if (strncmp("playpause", buf, num_bytes) == 0)
                scsender->PlayPause();
            else if (strncmp("restart", buf, num_bytes) == 0)
                scsender->Restart();
            else if (strncmp("codec", buf, num_bytes) == 0)
                response = scsender->CodecName();
            else if (strncmp("state", buf, num_bytes) == 0)
                response = scsender->Paused() ? "Stopped" : "Playing";
            else if (strncmp("enable", buf, num_bytes) == 0)
                sender->SetEnabled(true);
            else if (strncmp("disable", buf, num_bytes) == 0)
                sender->SetEnabled(false);
            else if (strncmp("multicast", buf, num_bytes) == 0)
                sender->SetMulticast(true);
            else if (strncmp("unicast", buf, num_bytes) == 0)
                sender->SetMulticast(false);

            ssize_t bytes_sent = sendto(g_sockfd, (const void *)response.c_str(),
                response.length(), 0, (struct sockaddr *)&client_addr, len);
            if (bytes_sent == -1) {
                LOGERR("Error: Cannot write to socket: " <<
                       strerror(errno) << endl);
            }
        }
    }

_close_socket:
    if (g_sockfd > 0)
        close(g_sockfd);
_leave:
    LOGDEB("SongcastSender: Leaving user command handler\n");
}

static char *thisprog;
static char usage [] =
"  -h, --help, show this help message and exit.\n"
"  -A, --audio, [44100:16:2:0/1] freq:bits:chans:swap.\n"
"    swap==1 if the input is little-endian. Set this only if the data can't be\n"
"    obtained from the file. Conflicting values will cause an error. \n"
"  -a, --adapter, [adapter] index of network adapter to use.\n"
"  -C, --codec, [PCM|FLAC] select streaming codec.\n"
"  -c, --channel, [0..65535] sender channel.\n"
"  -d, --disabled, [disabled] start up disabled.\n"
"  -f, --file, [file] file name to read and send.\n"
"      Use xx.wav for an actual wav,\n"
"      xx or xx.fifo for a fifo, stdin for stdin.\n"
"  -o, --output, [output] file name to write uri and metadata.\n"
"  -l, --latency, [latency] latency in ms.\n"
"  -m, --multicast, [multicast] use multicast instead of unicast.\n"
"  -p, --pace, Use internal timer to pace source. Implicit for regular files.\n"
"  -n, --name, [name] name of the sender.\n"
"  -t, --ttl, [ttl] ttl.\n"
"  -u, --udn, [udn] udn for the upnp device.\n"
	;
static void
Usage(FILE *fp = stderr)
{
    fprintf(fp, "%s: usage:\n%s", thisprog, usage);
    exit(1);
}

#include <getopt.h>
static int	   op_flags;
#define OPT_A    0x1
#define OPT_a    0x2
#define OPT_c    0x4
#define OPT_d    0x8
#define OPT_f    0x10
#define OPT_l    0x20
#define OPT_m    0x40
#define OPT_n    0x80
#define OPT_p    0x100
#define OPT_t    0x200
#define OPT_u    0x400
#define OPT_C    0x800

static struct option long_options[] = {
    {"audio", required_argument, 0, 'A'},
    {"adapter", required_argument, 0, 'a'},
    {"codec", required_argument, 0, 'C'},
    {"channel", required_argument, 0, 'c'},
    {"disabled", 0, 0, 'd'},
    {"file", required_argument, 0, 'f'},
    {"output", required_argument, 0, 'o'},
    {"latency", required_argument, 0, 'l'},
    {"multicast", 0, 0, 'm'},
    {"name", required_argument, 0, 'n'},
    {"pace", 0, 0, 'p'},
    {"ttl", required_argument, 0, 't'},
    {"udn", required_argument, 0, 'u'},
    {0, 0, 0, 0}
};

int main(int argc, char **argv)
{
    thisprog = argv[0];
    int ret;
    (void)op_flags;
    string audioparams, sfile, sname("Openhome WavSender"), sudn("12345678");
    string codec;
    string ofile;
    unsigned int adaptidx(0), channel(0), ttl(1), latency(100);
    while ((ret = getopt_long(argc, argv, "A:a:C:c:df:o:l:mn:pt:u:",
                              long_options, NULL)) != -1) {
        switch (ret) {
        case 'A': audioparams = optarg;op_flags |= OPT_A; break;
        case 'a': adaptidx = atoi(optarg);op_flags |= OPT_a; break;
        case 'C': codec = optarg;op_flags |= OPT_C; break;
        case 'c': channel = atoi(optarg);op_flags |= OPT_c; break;
        case 'd': op_flags |= OPT_d; break;
        case 'f': sfile = optarg;op_flags |= OPT_f; break;
        case 'o': ofile = optarg; break;
        case 'h': Usage(stdout);break;
        case 'l': latency = atoi(optarg); op_flags |= OPT_l; break;
        case 'm': op_flags |= OPT_m; break;
        case 'n': sname = optarg;op_flags |= OPT_n; break;
        case 'p': op_flags |= OPT_p; break;
        case 't': ttl = atoi(optarg);op_flags |= OPT_t; break;
        case 'u': sudn = optarg;op_flags |= OPT_u; break;
        default:
            Usage();
        }
    }
    if (optind != argc ) 
        Usage();
    if (sfile.empty())
        Usage();
    
    InitialisationParams* initParams = InitialisationParams::Create();

    Library* lib = new Library(initParams);

    std::vector<NetworkAdapter*>* subnetList = lib->CreateSubnetList();
    LOGDEB("adapter list:\n");
    for (unsigned i=0; i<subnetList->size(); ++i) {
        TIpAddress addr = (*subnetList)[i]->Address();
        LOGDEB("  " << i << ": " << (addr&0xff) << "." <<
               ((addr>>8)&0xff) << "." << ((addr>>16)&0xff) << "." <<
               ((addr>>24)&0xff) << endl);
    }
    if (subnetList->size() <= adaptidx) {
        LOGERR("ERROR: adapter " << adaptidx << "doesn't exist\n");
        return (1);
    }

    TIpAddress subnet = (*subnetList)[adaptidx]->Subnet();
    TIpAddress adapter = (*subnetList)[adaptidx]->Address();
    Library::DestroySubnetList(subnetList);
    lib->SetCurrentSubnet(subnet);

    LOGDEB("using subnet " << (subnet&0xff) << "." << ((subnet>>8)&0xff) << "."
           << ((subnet>>16)&0xff) << "." <<  ((subnet>>24)&0xff) << endl);

    Brhz file(sfile.c_str());

    Brhz udn(sudn.c_str());
    Brhz name(sname.c_str());
    TBool multicast = op_flags & OPT_m;
    TBool disabled = op_flags & OPT_d;
    TBool needpace = op_flags & OPT_p;

    AudioReader *audio = openAudio(sfile, audioparams, !needpace);
    if (!audio || !audio->open()) {
        cerr << "Audio file open failed" << endl;
        return 1;
    }
    needpace = !audio->isblocking();
    LOGDEB("sample rate:        " << audio->sampleRate() << endl);
    LOGDEB("sample size:        " << audio->bytesPerSample() << endl);
    LOGDEB("channels:           " << audio->numChannels() << endl);
    
    DvStack* dvStack = lib->StartDv();

    DvDeviceStandard* device = new DvDeviceStandard(*dvStack, udn);
    
    device->SetAttribute("Upnp.Domain", "av.openhome.org");
    device->SetAttribute("Upnp.Type", "Sender");
    device->SetAttribute("Upnp.Version", "1");
    device->SetAttribute("Upnp.FriendlyName", name.CString());
    device->SetAttribute("Upnp.Manufacturer", "Openhome");
    device->SetAttribute("Upnp.ManufacturerUrl", "http://www.openhome.org");
    device->SetAttribute("Upnp.ModelDescription", "Openhome WavSender");
    device->SetAttribute("Upnp.ModelName", "Openhome WavSender");
    device->SetAttribute("Upnp.ModelNumber", "1");
    device->SetAttribute("Upnp.ModelUrl", "http://www.openhome.org");
    device->SetAttribute("Upnp.SerialNumber", "");
    device->SetAttribute("Upnp.Upc", "");

    OhmSenderDriver* driver = new OhmSenderDriver(lib->Env());
    
    Brn icon(icon_png, icon_png_len);

    OhmSender* sender =
        new OhmSender(lib->Env(), *device, *driver, name, channel, adapter, ttl,
                      latency, multicast, !disabled, icon, Brn("image/png"), 0);
    SongcastSender* scsender = new SongcastSender(lib->Env(), sender, driver,
                                         file, audio, codec, needpace);
    
    device->SetEnabled();

    const Brx& suri(sender->SenderUri());
    string uri((const char*)suri.Ptr(), suri.Bytes());
    const Brx& smeta(sender->SenderMetadata());
    string meta((const char*)smeta.Ptr(), smeta.Bytes());

    ostream* fp = &cout;
    ofstream fout;
    if (!ofile.empty()) {
        fout.open(ofile);
        fp = &fout;
    }
    *fp << "URI " << UPnPP::base64_encode(uri) <<
        " METADATA " << UPnPP::base64_encode(meta) << endl << flush;

    signal(SIGUSR1, sigcatcher);
    signal(SIGINT, sigcatcher);
    signal(SIGTERM, sigcatcher);
    if (scsender->Start(!disabled)) {
        HandleUserCmd(sender, scsender);
        // If HandleUserCmd() fails with an error we do not want to exit, but
        // wait for termination.
        if (!g_quitrequest)
            pause();
        else
            // Allow the receivers to receive the halt flag we set When
            // we set g_quitrequest.
            sleep(1);
    }

    LOGDEB("Main: cleaning up\n");
    delete (scsender);

    delete (device);
    
    UpnpLibrary::Close();

    return (0);
}

/* Copyright (C) 2015-2018 J.F.Dockes
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "pcmencoder.h"
#include "audioutil.h"
#include "log.h"

#ifdef DEBUG_ENCODER
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#endif

using namespace std;

PcmEncoder::PcmEncoder(AudioReader* audioReader, OhmSenderDriver *ohmSender,
                       unsigned packetBytes)
    : AudioEncoder(audioReader, ohmSender, true, "PCM"),
      m_packetBytes(packetBytes)
{
    LOGDEB("New PCM encoder\n");

#ifdef DEBUG_ENCODER
    dumpfd = open("/tmp/mpd2dump.pcm", O_WRONLY|O_CREAT|O_TRUNC, 0666);
#endif
}

PcmEncoder::~PcmEncoder()
{
    LOGDEB("Delete PCM encoder\n");

#ifdef DEBUG_ENCODER
    if (dumpfd > 0)
        close(dumpfd);
#endif
}

void PcmEncoder::start()
{
    LOGDEB("Start PCM encoder\n");
}

int PcmEncoder::encode(const unsigned char *buffer, unsigned num_bytes, bool halt)
{
    if (num_bytes > 0) {
        unsigned missing_bytes = m_packetBytes - num_bytes;
        if (missing_bytes > 0) {
            LOGDEB("PcmEncoder::encode: inserting " << missing_bytes << " bytes\n");
            memset((void*)(buffer + num_bytes), 0, missing_bytes);
        }

        if (m_audioReader->needswap())
            swapSamples((unsigned char*)buffer, m_audioReader->bytesPerSample(),
                        m_packetBytes / m_audioReader->bytesPerSample());
    }

    m_ohmSender->SendAudio((const TByte*)buffer, (TUint)num_bytes, (TBool)halt);

#ifdef DEBUG_ENCODER
    if (dumpfd > 0)
        write(dumpfd, buffer, num_bytes);
#endif

    return num_bytes;
}

void PcmEncoder::finish()
{
    LOGDEB("Finish PCM encoder\n");
}

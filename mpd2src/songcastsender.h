/* Copyright (C) 2015 J.F.Dockes
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef _SONGCASTSENDER_H_INCLUDED_
#define _SONGCASTSENDER_H_INCLUDED_

#include <OpenHome/Os.h>

#ifdef WITH_OHBUILD
#include "OhmSender.h"
#else
#include <OpenHome/OhmSender.h>
#endif

#include "audioencoder.h"
#include "audioreader.h"
#include "log.h"

using namespace std;

using namespace OpenHome;
using namespace OpenHome::Av;

class SongcastSender {
public:
    static const TUint kPeriodMs = 10;
    static const TUint kSpeedNormal = 100;
    static const TUint kSpeedMin = 75;
    static const TUint kSpeedMax = 150;
    static const TUint kMaxPacketBytes = 4096;
        
public:
    SongcastSender(Environment& aEnv, OhmSender* aSender,
                   OhmSenderDriver* driver, const Brx& aUri,
                   AudioReader* audio, const string& codec, bool paced);
    bool Start(TBool aEnabled);
    void Play();
    void PlayPause();
    void Stop();
    void Restart();
    ~SongcastSender();
    void busyRdWr();
    TBool Paused();
    const string CodecName();
private:
    void CalculatePacketBytes();
    void TimerExpired();
        
private:
    Environment& iEnv;
    OhmSender* iSender;
    OhmSenderDriver* iDriver;
    Bws<OhmSender::kMaxTrackUriBytes> iUri;
    AudioReader *m_audio;
    AudioEncoder *m_encoder;
    Timer iTimer;
    Mutex iMutex;
    TBool iPaused;
    TUint iSpeed;           // percent, 100%=normal
    TUint iIndex;           // byte offset read position in source data
    TUint iPacketBytes;     // how many bytes of audio in each packet
    TUint iPacketFrames;    // how many audio frames in each packet
    TUint iPacketTime;      // how much audio time in each packet, uS
    TUint64 iLastTimeUs;    // last time stamp from system
    TInt32 iTimeOffsetUs;   // running offset in usec from ideal time
    //  <0 means sender is behind
    //  >0 means sender is ahead
    TBool iVerbose;
    TBool iPaced;
};

#endif /* _SONGCASTSENDER_H_INCLUDED_ */

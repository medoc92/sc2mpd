#!/bin/sh
# Copyright (C) 2016 J.F.Dockes
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the
#   Free Software Foundation, Inc.,
#   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


# Helper script for building the openhome libs prior to building sc2mpd and mpd2sc.
#
# Read about Openhome ohNet here: http://www.openhome.org/wiki/OhNet
#
# The source code we process is:
#    Copyright 2011-14, Linn Products Ltd. All rights reserved.
# See the license files under the different subdirs. In a nutshell: MIT
#
# This is far from foolproof or knowledgeable, but it seems to get me usable (static) libs.
#
# There are 4 modes of operation:
#   No option: just run the build in the parameter directory, without tweaking
#              anything in the source.
#   -c: clone, adjust, trim the source directories and produce a tar file
#   -b: clone, adjust the source dirs and build
#   -t: extract tar file and build (this is used mostly for packaging).
#
# When cloning, we checkout a known working version (more or less random, based on the
# last date I tried this successfully, sometimes more recent versions don't build), then
# apply some small patches mostly related to the build mechanism, and build in the
# different top directories.
#
# When producing the tar file, we get rid of the .git directory and a bunch of other
# things to reduce the size.


fatal()
{
    echo $*; exit 1
}

usage()
{
    echo "Usage:"
    echo "ohbuild.sh -c <topdir> : clone and adjust openhome directories"
    echo " from the git repositories, and produce tar file in /tmp"
    echo "ohbuild.sh -t <tarfile> <topdir> : extract tar file in top dir and"
    echo "  build openhome in there"
    echo "ohbuild.sh -b <topdir> : clone and build, no cleaning up of unused"
    echo "  files, no tar file"
    echo "ohbuild.sh <topdir> : just build, don't change the tree"
    exit 1
}

opt_t=0
opt_c=0
opt_b=0
tarfile=''

opts=`getopt -n ohbuild.sh -o t:cb -- "$@"`
eval set -- "$opts"

while true ; do
    case "$1" in
        -t) opt_t=1; tarfile=$2 ; shift 2 ;;
        -b) opt_b=1; shift ;;
        -c) opt_c=1; shift ;;
        --) shift ; break ;;
        *) usage ;;
    esac
done

echo opt_t $opt_t
echo opt_c $opt_c
echo opt_b $opt_b
echo tarfile $tarfile

test $# -eq 1 || usage
topdir=$1
echo topdir $topdir

# Only one of -tcb
tot=`expr $opt_t + $opt_c + $opt_b`
test $tot -le 1 || usage

arch=
debug=

# Note that srcdir needs actually be the source tree only in the case
# where we are patching the tree.
srcdir=`pwd`

test -d $topdir || mkdir $topdir || fatal "Can't create $topdir"

cd $topdir || exit 1
# Make topdir an absolute path
topdir=`pwd`

otherfiles=`echo *.*`
test "$otherfiles"  != '*.*' && fatal "topdir should not contain files (found $otherfiles)"

# Get patches from $srcdir/ohpatches/$1 and apply them. 
apply_patches()
{
    test -d ${srcdir}/ohpatches || \
   fatal "Can't find ohpatches directory? Must run in the top sc2mpd source dir"

    subdir=$1
    if test ! -d ${srcdir}/ohpatches/${subdir} ; then
        echo "-- No patches for $subdir --"
        return
    else
        echo "-- Patching in $subdir --"
    fi
    cd $topdir/$subdir/
    for p in ${srcdir}/ohpatches/${subdir}/*.patch;do
        patch -p1 < $p || exit 1
    done
}

gitclone_oh()
{
    echo "Cloning OpenHome from git repos into $topdir"
    cd $topdir
    for rep in \
        https://github.com/openhome/ohNet.git \
        https://github.com/openhome/ohdevtools.git \
        https://github.com/openhome/ohNetGenerated.git \
        https://github.com/openhome/ohTopology.git \
        https://github.com/joerg-krause/ohSongcast.git \
        ; do
        dir=`echo $rep | sed -e 's+https://github.com/.*/++' \
            -e 's/\.git$//'`
        echo $dir
        test ! -d $dir && git clone $rep
    done
}

clone_oh()
{
#    gitclone_oh
    
    cd $topdir/ohNet
    #  ohNet_1.17.2857, Mon Feb 19 00:40:18 2018 +0000
    git checkout 07831fe44c45ccde939ceaa4e99e46d2dbd2cf4e || exit 1
    git checkout .
    apply_patches ohNet

    cd  $topdir/ohNetGenerated
    # Tue May 9 08:54:47 2017 
    git checkout e3edb912410d4c5a4d5323bb1e9c27660a42d78f || exit 1
    git checkout .
    apply_patches ohNetGenerated

    cd  $topdir/ohTopology
    # Wed Mar 22 11:15:28 2017 +0000
    git checkout cc09c09da4be8d3d04adae5b8f0daaf8450906a3 || exit 1
    git checkout .
    apply_patches ohTopology

    cd  $topdir/ohSongcast
    # Wed May 30 15:56:56 2018 +0200
    git checkout 9d886a67d08d42923537773b007dd7df11e52521 || exit 1
    git checkout .
    apply_patches ohSongcast
}

make_tarfile()
{
    cd $topdir || exit 1
    
    # Make space: get rid of the .git and other not useful data, then
    # produce a tar file for reproduction
    for dir in ohNet ohNetGenerated ohdevtools ohTopology ohSongcast;do
        test -d $dir || fatal no "'$dir'" in "'$topdir'"
    done
    #rm -rf $topdir/ohNet/thirdparty
    rm -rf $topdir/ohNetGenerated/OpenHome/Net/Bindings/Cs
    rm -rf $topdir/ohNetGenerated/OpenHome/Net/Bindings/Java
    rm -rf $topdir/ohNetGenerated/OpenHome/Net/Bindings/Js
    rm -rf $topdir/ohNetGenerated/OpenHome/Net/T4/
    rm -rf $topdir/ohSongcast/Docs/
    rm -rf $topdir/ohSongcast/ohSongcast/Mac
    rm -rf $topdir/ohSongcast/ohSongcast/Windows
    rm -rf $topdir/ohTopology/waf
    rm -rf $topdir/ohdevtools/nuget
    
    dt=`date +%Y%m%d`
    tar --exclude='.git' -czf $tarfile/tmp/openhome-sc2-${dt}.tar.gz .
    echo "Built $tarfile/tmp/openhome-sc2-${dt}.tar.gz"
}

build_ohNet()
{
    dir=ohNet
    echo;echo building $dir
    cd  $topdir/$dir || exit 1

    make native_only=yes || exit 1

    cd ..
}

build_ohNetGenerated()
{
    dir=ohNetGenerated
    echo;echo building $dir
    cd  $topdir/$dir || exit 1

    # e.g. Linux-x64, Linux-armhf
    arch=`basename $topdir/ohNet/Build/Bundles/ohNet-*-*.tar.gz | \
        sed -e s/ohNet-//  -e s/-[A-Z][a-z][a-z]*\.tar\.gz$//`
    # e.g. Debug, Release
    debug=`basename $topdir/ohNet/Build/Bundles/ohNet-*-*.tar.gz | \
        sed -e s/.*-//  -e s/\.tar\.gz$//`

    sd=dependencies/${arch}
    mkdir -p "$sd"
    (cd $sd;
        tar xvzf $topdir/ohNet/Build/Bundles/ohNet-${arch}-${debug}.tar.gz
    ) || exit 1


    make native_only=yes
    # Expect error here, but the relevant bits have been built
    
    # Copy the includes from here to the ohNet dir where ohTopology
    # will want them
    tar cf - Build/Include | (cd $topdir/ohNet/;tar xvf -) || exit 1
}

build_ohdevtools()
{
    dir=ohdevtools
    echo;echo building $dir
    cd  $topdir/$dir || exit 1

    # Nothing to build
}


# It appears that nothing compiled in topology is needed for Receivers
# or Senders, only managers of those. Some of the include files are
# needed (or at least used) though.
build_ohTopology()
{
    dir=ohTopology
    echo;echo building $dir
    cd  $topdir/$dir || exit 1

    #./go fetch --all --clean 
    #./waf configure --ohnet=../ohNet --dest-platform=Linux-x86

    # The build fails because of mono issues (trying to generate
    # include files from templates, this is probably fixable as the e
    # actual includes may exist somewhere).
    #./waf build

    mkdir -p build/Include/OpenHome/Av
    cp -p OpenHome/Av/*.h build/Include/OpenHome/Av/
}

build_ohSongcast()
{
    dir=ohSongcast
    echo;echo building $dir
    cd  $topdir/$dir || exit 1

    # 2017 and older
    #make release=1 Receiver WavSender
    #ar r Build/Obj/Posix/Release/libohSongcast.a Build/Obj/Posix/Release/*.o
    # 2018
    make release=1 library_static
}

official_way()
{
    # from README, actually does not work, for reference. Issues probably have
    # something to do with lacking mono or wrong version
    cd ohNet
    make ohNetCore proxies devices TestFramework
    cd ../ohNetGenerated
    ./go fetch --all
    make
    cd ../ohNetmon
    ./go fetch --all
    ./waf configure --ohnet=../ohNet
    ./waf build
    cd ../ohTopology
    ./go fetch --all
    ./waf configure --ohnet=../ohNet
    ./waf build
    cd ../ohSongcast
    make release=1
}

buildall()
{
    echo "Building all in $topdir"
    build_ohNet
    build_ohNetGenerated
    build_ohdevtools
    build_ohTopology
    build_ohSongcast
}

if test $opt_c -ne 0; then
#    test -d $topdir/ohNet && fatal target dir should be initially empty \
#                                   for producing a tar distribution
    clone_oh || fatal clone failed
    make_tarfile || fatal make_tarfile failed
    exit 0
fi

# Extract tar, or clone git repos
if test $opt_t -eq 1; then
    echo "Extracting tarfile in $topdir"
    cd $topdir || exit 1
    tar xf $tarfile
elif test $opt_b -eq 1; then
    clone_oh
fi

buildall

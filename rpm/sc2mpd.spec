Summary:        Gateway forwarding a Linn Songcast audio stream to MPD
Name:           sc2mpd
Version:        1.1.4
Release:        1%{?dist}
Group:          Applications/Multimedia
License:        GPLv2+
URL:            http://www.lesbonscomptes.com/recoll/
Source0:        http://www.lesbonscomptes.com/upmpdcli/downloads/sc2mpd-%{version}.tar.gz
Source1:        http://www.lesbonscomptes.com/upmpdcli/downloads/openhome-sc2-20170919.tar.gz
Patch0:         scmakempdsender_explicit_python3.patch
BuildRequires:  libmicrohttpd-devel
BuildRequires:  libsamplerate-devel
BuildRequires:  alsa-lib-devel

%description
sc2mpd is designed to work with upmpdcli, with which it provides a
Songcast Receiver interface usable from any Songcast Sender such as
the Windows or Mac OS X Linn Songcast applications.  The package also
now contains a reverse gateway, mpd2sc, forwarding audio played by mpd
to a Songcast Sender, mostly useful for multiroom synchronized play.


%prep
%setup -q -n %{name}-%{version}
%patch0 -p1
mkdir openhome
cd openhome
gzip -dc %{SOURCE1} | tar -xvvf -

%build
CFLAGS="%{optflags}"; export CFLAGS
CXXFLAGS="%{optflags}"; export CXXFLAGS
LDFLAGS="%{?__global_ldflags}"; export LDFLAGS
sh ./ohbuild.sh openhome

%configure --with-openhome=openhome
make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot} STRIP=/bin/true INSTALL='install -p'

%files
%{_bindir}/%{name}
%{_bindir}/mpd2sc
%{_bindir}/scmakempdsender

%changelog
* Wed Mar 15 2017 Jean-Francois Dockes <jf@dockes.org> 1.1.1-1
- initial packaging

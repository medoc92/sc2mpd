/* Copyright (C) 2014-2024 J.F.Dockes
 *       This program is free software; you can redistribute it and/or modify
 *       it under the terms of the GNU General Public License as published by
 *       the Free Software Foundation; either version 2 of the License, or
 *       (at your option) any later version.
 *
 *       This program is distributed in the hope that it will be useful,
 *       but WITHOUT ANY WARRANTY; without even the implied warranty of
 *       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *       GNU General Public License for more details.
 *
 *       You should have received a copy of the GNU General Public License
 *       along with this program; if not, write to the
 *       Free Software Foundation, Inc.,
 *       59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
#include "config.h"

#include <string.h>
#include <sys/types.h>
#include <math.h>

#ifdef WORDS_BIGENDIAN
#if HAVE_BYTESWAP_H
#include <byteswap.h>
#define BSWAP16(X) bswap_16(X)
#else
#define BSWAP16(X) ((((X) & 0xff) >> 8) | ((X) << 8))
#endif
#else // Little endian -> 
#define BSWAP16(X) (X)
#endif

#include <iostream>
#include <queue>
#include <mutex>
#include <alsa/asoundlib.h>

#include <samplerate.h>

#include "log.h"
#include "rcvqueue.h"
#include "conftree.h"

using namespace std;

#ifndef MIN
#define MIN(A, B) ((A) < (B) ? (A) : (B))
#endif

float g_volumescale{1.0};

// Max absolute variation of the adjusted samplerate
static const double max_delta{0.01};

// The queue for audio blocks ready for alsa. This is the maximum size
// before the upstream task blocks. Songcast normally uses 10 mS audio blocks.
static const unsigned int qs_hi = 100;

// Queue size target in 10mS blocks, including alsa buffers. Can be
// set (in mS) with scqstargms
static unsigned int qstarg{50};

// Alsa parameters:
// 
// A period is data processed between interrupts. When playing,
// there is one period belonging to the hardware and normally
// others that the software can fill up. The minimum reasonable is
// 2 periods (one for us, one for the hardware), which we try to
// use as this gives minimum latency while being workable.
//
// We try to control this so that the delay is constant in all
// instances, independantly of the local hardware defaults. But we
// have to accept that the driver may have other constraints.
//
// These are in uS and can be changed (in mS) with scbuffertimems and
// scperiodtimems
static unsigned int buffer_time = 200000;
static unsigned int period_time =  50000;

static WorkQueue<AudioMessage*> alsaqueue("alsaqueue", qs_hi);

/* This is used to disable sample rate conversion until playing is actually 
   started */
static bool qinit = false;
static bool audio_stopped = false;

/* Alsa. Locked while we work with it */
static std::mutex alsa_mutex;
static snd_pcm_t *pcm;
static string alsadevice("default");
// Current delay from top of buffer to DAC in frames, retrieved from alsa.
static snd_pcm_sframes_t alsa_delay;

static void alsa_close_nolock();

// From MPD recovery code.
// Note: no locking: we're called from alsawriter holding the lock.
static int alsa_recover(snd_pcm_t *pcm, int err)
{
    if (err == -EPIPE) {
        LOGDEB("Underrun on ALSA device\n");
    } else if (err == -ESTRPIPE) {
        LOGDEB("ALSA device was suspended\n");
    }

    switch (snd_pcm_state(pcm)) {
    case SND_PCM_STATE_PAUSED:
        err = snd_pcm_pause(pcm, /* disable */ 0);
        break;
    case SND_PCM_STATE_SUSPENDED:
        err = snd_pcm_resume(pcm);
        if (err == -EAGAIN)
            return 0;
        /* fall-through to snd_pcm_prepare: */
    case SND_PCM_STATE_SETUP:
    case SND_PCM_STATE_XRUN:
        //ad->period_position = 0;
        err = snd_pcm_prepare(pcm);
        break;
    case SND_PCM_STATE_DISCONNECTED:
        break;
        /* this is no error, so just keep running */
    case SND_PCM_STATE_RUNNING:
        err = 0;
        break;
    default:
        /* unknown state, do nothing */
        break;
    }

    return err;
}

// Set after initializing the driver
static snd_pcm_uframes_t periodframes;
static snd_pcm_uframes_t bufferframes;

static bool alsa_init(const string& dev, AudioMessage *tsk);

static void *alsawriter(void *p)
{
    while (true) {
        if (!qinit) {
            if (!alsaqueue.waitminsz(qstarg)) {
                LOGERR("alsawriter: waitminsz failed\n");
                alsaqueue.workerExit();
                return (void *)1;
            }
        }

        AudioMessage *tsk = 0;
        if (!alsaqueue.take(&tsk)) {
            // TBD: reset alsa?
            alsaqueue.workerExit();
            return (void*)1;
        }

        std::unique_lock<std::mutex> lock(alsa_mutex);
        if (pcm == nullptr) {
            if (!alsa_init(alsadevice, tsk)) {
                alsaqueue.workerExit();
                return (void*)1;
            }
        }

        snd_pcm_uframes_t frames = tsk->frames();
        char *buf = tsk->m_buf;
        // This loop is copied from the alsa sample, but it should not
        // be necessary, in synchronous mode, alsa is supposed to
        // perform complete writes except for errors or interrupts
        while (frames > 0) {
            if (g_quitrequest) {
                break;
            }
            if (snd_pcm_delay(pcm, &alsa_delay) < 0) {
                alsa_delay = 0;
            }
            LOGDEB1("alsawriter: avail frms " << snd_pcm_avail(pcm) << " writing " << frms << '\n');
            snd_pcm_sframes_t ret =  snd_pcm_writei(pcm, tsk->m_buf, frames);
            if (ret != int(frames)) {
                LOGERR("snd_pcm_writei(" << frames <<" frames) failed: ret: " << ret << '\n');
            } else {
                qinit = true;
            }
            if (ret == -EAGAIN) {
                LOGDEB("alsawriter: EAGAIN\n");
                continue;
            }
            if (ret <= 0) {
                if (alsa_recover(pcm, ret) < 0) {
                    LOGERR("alsawriter: write and recovery failed: " << ret << '\n');
                    alsaqueue.workerExit();
                    return (void*)1;
                }
                qinit = false;
                break;
            }

            buf += tsk->frames_to_bytes(ret);
            frames -= ret;
        }
        if (tsk->m_halt) {
            LOGDEB("alsawriter: halt\n");
            alsa_close_nolock();
            qinit = false;
        }
        if (audio_stopped && alsaqueue.qsize() == 0) {
            LOGDEB("alsawriter: stopped, alsa queue empty\n");
            alsa_close_nolock();
            qinit = false;
        }

        delete tsk;
    }
}

static bool alsa_init(const string& dev, AudioMessage *tsk)
{
    int err;
    const char *cmd = "";

    if ((err = snd_pcm_open(&pcm, dev.c_str(), SND_PCM_STREAM_PLAYBACK, 0)) < 0) {
        LOGERR("alsa_init: snd_pcm_open " << dev << " " << snd_strerror(err) << '\n');
        return false;;
    }

    snd_pcm_hw_params_t *hwparams;
    snd_pcm_hw_params_alloca(&hwparams);

    cmd = "snd_pcm_hw_params_any";
    if ((err = snd_pcm_hw_params_any(pcm, hwparams)) < 0) {
        goto error;
    }
    cmd = "snd_pcm_hw_params_set_access";
    if ((err = snd_pcm_hw_params_set_access(pcm, hwparams, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0) {
        goto error;
    }

    cmd = "snd_pcm_hw_params_set_format";
    if ((err = snd_pcm_hw_params_set_format(pcm, hwparams, SND_PCM_FORMAT_S16_LE)) < 0) {
        goto error;
    }
    cmd = "snd_pcm_hw_params_set_channels";
    if ((err = snd_pcm_hw_params_set_channels(pcm, hwparams, tsk->m_chans)) < 0) {
        goto error;
    }
    cmd = "snd_pcm_hw_params_set_rate_near";
    unsigned int actual_rate;
    actual_rate = tsk->m_freq;
    if ((err = snd_pcm_hw_params_set_rate_near(pcm, hwparams, &actual_rate, 0)) < 0) {
        goto error;
    }
    if (actual_rate != tsk->m_freq) {
        LOGERR("snd_pcm_hw_params_set_rate_near: got actual rate " << actual_rate << '\n');
        goto error;
    }

    // Note: we don't use these values, get them just for information purposes
    unsigned int periodsmin, periodsmax;
    snd_pcm_hw_params_get_periods_min(hwparams, &periodsmin, 0);
    snd_pcm_hw_params_get_periods_max(hwparams, &periodsmax, 0);
    snd_pcm_uframes_t bsmax, bsmin, prmin, prmax;
    snd_pcm_hw_params_get_buffer_size_min(hwparams, &bsmin);
    snd_pcm_hw_params_get_buffer_size_max(hwparams, &bsmax);
    snd_pcm_hw_params_get_period_size_min(hwparams, &prmin, 0);
    snd_pcm_hw_params_get_period_size_max(hwparams, &prmax, 0);
    LOGINF("Alsa: periodsmin " << periodsmin << " periodsmax " << periodsmax <<
           " bsminsz " << bsmin << " bsmaxsz " << bsmax << 
           " prminsz " << prmin << " prmaxsz " << prmax << '\n');

    cmd = "snd_pcm_hw_params_set_buffer_time_near";
    unsigned int buftimereq;
    buftimereq = buffer_time;
    if ((err = snd_pcm_hw_params_set_buffer_time_near(pcm, hwparams,
                                                      &buffer_time, 0)) < 0) {
        goto error;
    }
    LOGINF("Alsa: set buffer_time_near: asked " << buftimereq << " got " << buffer_time << '\n');

    cmd = "snd_pcm_hw_params_set_period_time_near";
    buftimereq = period_time;
    if ((err = snd_pcm_hw_params_set_period_time_near(pcm, hwparams,
                                                      &period_time, 0)) < 0) {
        goto error;
    }
    LOGINF("Alsa: set_period_time_near: asked " << buftimereq << " got " << period_time << '\n');

    snd_pcm_hw_params_get_period_size(hwparams, &periodframes, 0);
    snd_pcm_hw_params_get_buffer_size(hwparams, &bufferframes);
    LOGDEB("Alsa: bufferframes " << bufferframes << " periodframes " << periodframes << '\n');
    
    cmd = "snd_pcm_hw_params";
    if ((err = snd_pcm_hw_params(pcm, hwparams)) < 0) {
        goto error;
    }
        
    /* configure SW params */
    snd_pcm_sw_params_t *swparams;
    snd_pcm_sw_params_alloca(&swparams);

    cmd = "snd_pcm_sw_params_current";
    err = snd_pcm_sw_params_current(pcm, swparams);
    if (err < 0)
        goto error;

    cmd = "snd_pcm_sw_params_set_start_threshold";
    err = snd_pcm_sw_params_set_start_threshold(pcm, swparams,
                                                bufferframes - periodframes);
    if (err < 0)
        goto error;

    cmd = "snd_pcm_sw_params_set_avail_min";
    err = snd_pcm_sw_params_set_avail_min(pcm, swparams, periodframes);
    if (err < 0)
        goto error;

    cmd = "snd_pcm_sw_params";
    err = snd_pcm_sw_params(pcm, swparams);
    if (err < 0)
        goto error;

    return true;

error:
    LOGERR("alsa_init: " << cmd << " error:" << snd_strerror(err) << '\n');
    alsa_close_nolock();
    return false;
}

void alsa_close()
{
    std::unique_lock<std::mutex> lock(alsa_mutex);
    alsa_close_nolock();
}

static void alsa_close_nolock()
{
    LOGDEB("alsawriter: alsa close\n");
    alsa_delay = 0;
    if (pcm != nullptr) {
        snd_pcm_close(pcm);
        pcm = nullptr;
    }
}

class Filter {
public:
#define FNS 128
    Filter() : old(0.0), sum(0.0), idx(0) {
        for (int i = 0; i < FNS; i++) {
            buf[i] = 1.0;
            sum += buf[i];
        }
    }
    double operator()(double ns) {
        buf[idx++] = ns;
        sum += ns;
        if (idx == FNS)
            idx = 0;
        sum -= buf[idx];
        return sum/FNS;
    }
    double old;
    double buf[FNS];
    double sum;
    int idx;
};

// Convert config parameter to libsamplerate converter type
// Hopefully this will never include neg values, as we use -1 to mean
// "no conversion"
static int src_cvt_type(ConfSimple *config)
{
    int tp = SRC_SINC_FASTEST;
    if (!config)
        return tp;
    string value;
    if (!config->get("sccvttype", value))
        return tp;
    LOGDEB("src_cvt_type. conf string [" << value << "]\n");
    if (!value.compare("SRC_SINC_BEST_QUALITY")) {
        tp = SRC_SINC_BEST_QUALITY;
    } else if (!value.compare("SRC_SINC_MEDIUM_QUALITY")) {
        tp = SRC_SINC_MEDIUM_QUALITY;
    } else if (!value.compare("SRC_SINC_FASTEST")) {
        tp = SRC_SINC_FASTEST;
    } else if (!value.compare("SRC_ZERO_ORDER_HOLD")) {
        tp = SRC_ZERO_ORDER_HOLD;
    } else if (!value.compare("SRC_LINEAR")) {
        tp = SRC_LINEAR;
    } else if (!value.compare("NONE")) {
        tp = -1;
    } else {
        // Allow numeric values for transparent expansion to
        // hypothetic libsamplerate updates (allowing this is explicit
        // in the libsamplerate doc).
        long int lval;
        char *cp;
        lval = strtol(value.c_str(), &cp, 10);
        if (cp != value.c_str()) {
            tp = int(lval);
        } else {
            LOGERR("Invalid converter type [" << value << "] using SRC_SINC_FASTEST" << '\n');
        }
    }
    return tp;
}

// Computing the samplerate conversion factor. We want to keep
// the queue at its target size to control the delay. The
// present hack sort of works but could probably benefit from
// a more scientific approach
static double compute_ratio(double& samplerate_ratio, int bufframes,
                            Filter& filter)
{
    // Integral term. We do not use it at the moment
    // double it = 0;

    double qs = 0.0;
    
    if (qinit) {
        // Qsize in frames. This is the variable to control
        qs = alsaqueue.qsize() * bufframes + alsa_delay;
        // Error term
        double qstargframes = qstarg * bufframes;
        double et =  ((qstargframes - qs) / qstargframes);

        // Integral. Not used, made it worse each time I tried.
        // This is probably because our command is actually the
        // derivative of the error? I should try a derivative term
        // instead?
        // it += et;

        // Error correction coef
        double ce = 0.1;

        // Integral coef
        //double ci = 0.0001;

        // Compute command
        double adj = ce * et /* + ci * it*/;

        // Also tried a quadratic correction, worse.
        // double adj = et * ((et < 0) ? -et : et);

        // Computed ratio
        samplerate_ratio =  1.0 + adj;

        // Limit extension
        if (samplerate_ratio < 1.0 - max_delta) {
            samplerate_ratio = 1.0 - max_delta;
        } else if (samplerate_ratio > 1.0 + max_delta) {
            samplerate_ratio = 1.0 + max_delta;
        }

    } else {
        // Starting up, wait for more info
        qs = alsaqueue.qsize();
        samplerate_ratio = 1.0;
        // it = 0;
    }

    // Average the rate value to eliminate fast oscillations
    samplerate_ratio = filter(samplerate_ratio);
    return qs;
}

// Convert ints input buffer into floats for libsamplerate processing
// Data always comes in host order, because this is what we
// request from upstream. 24 and 32 bits are untested.
//
// Note about volumescale: special-casing the 1.0 value outside the
// loops yields a modest performance increase in a test program
// (around 4%), compared to always multiplying, and returns the perf
// to the one of the previous version which did not scale the volume
// at all. This ensures that we don't break existing installations
// with the scaling version.
// 
// Testing inside the loops, would use less code lines, but does
// not improve perf in the 1.0 case, and makes it worse in the not 1.0
// one.
static bool fixToFloats(AudioMessage *tsk, SRC_DATA& src_data,
                        size_t tot_samples)
{

    // For some reason, newer versions of libsamplerate define
    // data_in as const
    float *datain = (float *)&(src_data.data_in[0]);
    switch (tsk->m_bits) {
    case 16: 
    {
        const short *sp = (const short *)tsk->m_buf;
        if (g_volumescale == 1.0) {
            for (unsigned int i = 0; i < tot_samples; i++) {
                datain[i] = *sp++;
            }
        } else {
            for (unsigned int i = 0; i < tot_samples; i++) {
                datain[i] = g_volumescale * *sp++;
            }
        }
    }
    break;
    case 24: 
    {
        const unsigned char *icp = (const unsigned char *)tsk->m_buf;
        int o;
        unsigned char *ocp = (unsigned char *)&o;
        if (g_volumescale == 1.0) {
            for (unsigned int i = 0; i < tot_samples; i++) {
                ocp[0] = *icp++;
                ocp[1] = *icp++;
                ocp[2] = *icp++;
                ocp[3] = (ocp[2] & 0x80) ? 0xff : 0;
                datain[i] = o;
            }
        } else {
            for (unsigned int i = 0; i < tot_samples; i++) {
                ocp[0] = *icp++;
                ocp[1] = *icp++;
                ocp[2] = *icp++;
                ocp[3] = (ocp[2] & 0x80) ? 0xff : 0;
                datain[i] = g_volumescale * o;
            }
        }            
    }
    break;
    case 32: 
    {
        const int *ip = (const int *)tsk->m_buf;
        if (g_volumescale == 1.0) {
            for (unsigned int i = 0; i < tot_samples; i++) {
                datain[i] = *ip++;
            }
        } else {
            for (unsigned int i = 0; i < tot_samples; i++) {
                datain[i] = g_volumescale * *ip++;
            }
        }            
    }
    break;
    default:
        LOGERR("audioEater:alsa: bad m_bits: " << tsk->m_bits << '\n');
        return false;
    }
    return true;
}

// Convert floats buffer into output which is always 16LE for now. We
// should probably dither the lsb ?
// The libsamplerate output values can overshoot the input range (see
// http://www.mega-nerd.com/SRC/faq.html#Q001), so we take care to
// clip the values.
bool floatsToFix(AudioMessage *tsk, SRC_DATA& src_data,
                 size_t tot_samples)
{
    short *sp = (short *)tsk->m_buf;
    switch (tsk->m_bits) {
    case 16:
        for (unsigned int i = 0; i < tot_samples; i++) {
            int v = src_data.data_out[i];
            if (v > 32767) {
                v = 32767;
            } else if (v < -32768) {
                v = -32768;
            }
            *sp++ = BSWAP16(short(v));
        }
        break;
    case 24:
        for (unsigned int i = 0; i < tot_samples; i++) {
            int v = src_data.data_out[i];
            if (v > (1 << 23) - 1) {
                v = (1 << 23) - 1;
            } else if (v < -(1 << 23)) {
                v = -(1 << 23);
            }
            *sp++ = BSWAP16(short(v >> 8));
        }
        break;
    case 32:
        for (unsigned int i = 0; i < tot_samples; i++) {
            float& f = src_data.data_out[i];
            int v = f;
            if (f > 0 && v < 0) {
                v = unsigned(1 << 31) - 1;
            } else if (f < 0 && v > 0) {
                v = -unsigned(1 << 31);
            }
            *sp++ = BSWAP16(short(v >> 16));
        }
        break;
    default:
        LOGERR("audioEater:alsa: bad m_bits: " << tsk->m_bits << '\n');
        return false;
    }
    tsk->m_bytes = (char *)sp - tsk->m_buf;
    tsk->m_bits = 16;
    return true;
}

// Convert input buffer to 16le. Input samples are 16 bits or more, in
// host order. Data can only shrink, no allocation needed.
bool convert_to16le(AudioMessage *tsk)
{
    unsigned int tot_samples = tsk->samples();
    short *sp = (short *)tsk->m_buf;
    switch (tsk->m_bits) {
    case 16:
        for (unsigned int i = 0; i < tot_samples; i++) {
            short v = *sp;
            *sp++ = BSWAP16(v);
        }
        break;
    case 24:
    {
        const unsigned char *icp = (const unsigned char *)tsk->m_buf;
        for (unsigned int i = 0; i < tot_samples; i++) {
            int o;
            unsigned char *ocp = (unsigned char *)&o;
            ocp[0] = *icp++;
            ocp[1] = *icp++;
            ocp[2] = *icp++;
            ocp[3] = (ocp[2] & 0x80) ? 0xff : 0;
            *sp++ = BSWAP16(short(o >> 8));
        }
    }
    break;
    case 32:
    {
        const int *ip = (const int *)tsk->m_buf;
        for (unsigned int i = 0; i < tot_samples; i++) {
            *sp++ = BSWAP16(short((*ip++) >> 16));
        }
    }
    break;
    default:
        LOGERR("audioEater:alsa: bad m_bits: " << tsk->m_bits << '\n');
        return false;
    }
    tsk->m_bytes = (char *)sp - tsk->m_buf;
    tsk->m_bits = 16;
    return true;
}

// Complete input processing:
// - compute samplerate conversion factor,
// - convert input to float
// - apply conversion
// - Convert back to int16le
bool stretch_buffer(AudioMessage *tsk,
                    SRC_STATE * src_state, SRC_DATA& src_data,
                    size_t& src_input_bytes, Filter& filter)
{
    // Number of frames per buffer. This is mostly constant for a
    // given stream (depends on fe and buffer time, Windows Songcast
    // buffers are 10mS, so 441 frames at cd q). 
    int bufframes = tsk->frames();

    double samplerate_ratio = 1.0;
    unsigned int tot_samples = tsk->samples();

    // Compute sample rate ratio, and return current qsize in
    // frames. This is the variable which we control. Note that this
    // takes a non-const ret to samplerate_ration and changes it
    double qs = compute_ratio(samplerate_ratio, bufframes, filter);

    src_data.input_frames = tsk->frames();

    // Possibly reallocate buffer
    size_t needed_bytes = tot_samples * sizeof(float);
    if (src_input_bytes < needed_bytes) {
        src_data.data_in =
            (float *)realloc((void *)src_data.data_in, needed_bytes);
        src_data.data_out = (float *)realloc(src_data.data_out,
                                             2 * needed_bytes);
        src_data.output_frames = 2 * tot_samples / tsk->m_chans;
        src_input_bytes = needed_bytes;
    }

    src_data.src_ratio = samplerate_ratio;
    src_data.end_of_input = 0;

    // Convert to floats
    if (!fixToFloats(tsk, src_data, tot_samples)) {
        return false;
    }

    // Call samplerate converter
    int ret = src_process(src_state, &src_data);
    if (ret) {
        LOGERR("src_process: " << src_strerror(ret) << '\n');
        return false;
    }

    { // Tell the world
        static int cnt;
        if (cnt++ == 103) {
            LOGDEB("audioEater:alsa: " 
                   " qstarg " << qstarg <<
                   " iqsz " << alsaqueue.qsize() <<
                   " qsize " << int(qs/bufframes) << 
                   " ratio " << samplerate_ratio <<
                   " in " << src_data.input_frames << 
                   " consumed " << src_data.input_frames_used << 
                   " out " << src_data.output_frames_gen << '\n');
            cnt = 0;
        }
    }

    // New number of samples after conversion. We are going to
    // copy them back to the audio buffer, and may need to
    // reallocate it.
    tot_samples =  src_data.output_frames_gen * tsk->m_chans;
    needed_bytes = tot_samples * (tsk->m_bits / 8);
    if (tsk->m_allocbytes < needed_bytes) {
        tsk->m_allocbytes = needed_bytes;
        tsk->m_buf = (char *)realloc(tsk->m_buf, tsk->m_allocbytes);
        if (!tsk->m_buf) {
            LOGERR("audioEater:alsa: out of memory\n");
            return false;
        }
    }

    if (!floatsToFix(tsk, src_data, tot_samples)) {
        return false;
    }
    return true;
}

// Take data out of songcast, possibly stretch it and send it to the
// alsa queue.  We are run by a separate thread and normally never
// return.
static void *audioEater(void *cls)
{
    AudioEater::Context *ctxt = (AudioEater::Context*)cls;

    int cvt_type = src_cvt_type(ctxt->config);
    LOGDEB("audioEater: alsadirect. Will use converter type " << cvt_type << '\n');

    ctxt->config->get("scalsadevice", alsadevice);
    // Delay target in 10 mS blocks. This is specified in mS in the config
    qstarg = ctxt->config->getInt("scqstargms", qstarg * 10) / 10;
    // Buffer time and period time in uS. These are specified in mS in the config
    buffer_time = ctxt->config->getInt("scbuffertimems",buffer_time/1000) * 1000;
    period_time = ctxt->config->getInt("scperiodtimems",period_time/1000) * 1000;
    
    WorkQueue<AudioMessage*> *queue = ctxt->queue;

    delete ctxt;
    ctxt = 0;

    qinit = false;

    Filter filter;

    int src_error = 0;
    SRC_STATE *src_state = 0;
    SRC_DATA src_data;
    memset(&src_data, 0, sizeof(src_data));
    // Current size of the samplerate input buffer. We always alloc
    // twice the size for output (allocated on first use).
    size_t src_input_bytes = 0;
    
    alsaqueue.start(1, alsawriter, 0);

    while (true) {
        if (g_quitrequest) {
            goto done;
        }
        AudioMessage *tsk = 0;
        // Get new data
        if (!queue->take(&tsk)) {
            LOGDEB("audioEater: alsadirect: queue take failed\n");
            goto done;
        }
        alsaAudioEater.pktcounter++;
        
        if (tsk->m_bytes == 0 || tsk->m_chans == 0 || tsk->m_bits == 0) {
            LOGDEB("Zero buf\n");
            if (tsk->m_halt)
                goto put_audio_message;
            else
                continue;
        }

        // 1st time: init. We don't want to do this before we have data.
        if (src_state == 0) {
            if (cvt_type != -1) {
                src_state = src_new(cvt_type, tsk->m_chans, &src_error);
            } else {
                src_state = (SRC_STATE *)malloc(1);
            }
        }
        
        // Process input buffer
        if (cvt_type != -1) {
            if (!stretch_buffer(tsk, src_state, src_data, src_input_bytes, filter)) {
                goto done;
            }
        } else {
            static int cnt;
            if (cnt++ == 103) {
                int bufframes = tsk->frames()?tsk->frames():441;
                int qs = alsaqueue.qsize() * bufframes + alsa_delay;
                LOGDEB("audioEater:alsa: " 
                       " qstarg " << qstarg <<
                       " iqsz " << alsaqueue.qsize() <<
                       " qsize " << int(qs/bufframes) << '\n');
                cnt = 0;
            }
            convert_to16le(tsk);
        }

    put_audio_message:
        // Send data on its way
        if (!alsaqueue.put(tsk)) {
            LOGERR("alsaEater: queue put failed\n");
            goto alsaerror;
        }
    }


done:
    alsaqueue.setTerminateAndWait();
alsaerror:
    queue->workerExit();
    alsa_close();

    if (src_state) {
        if (cvt_type != -1) {
            src_delete(src_state);
        } else {
            free(src_state);
        }
    }
    free((void *)src_data.data_in);
    free(src_data.data_out);
    LOGDEB("audioEater returning\n");
    return (void *)1;
}

// Map the ALSA state to the audio eater state.
static AudioEater::AudioState audioState()
{
    AudioEater::AudioState state = AudioEater::AudioState::STOPPED;

    if (pcm != nullptr) {
        snd_pcm_state_t pcm_state = snd_pcm_state(pcm);
        LOGDEB("alsaEater: PCM state " << pcm_state << '\n');
        if (pcm_state == SND_PCM_STATE_RUNNING) {
            state = AudioEater::AudioState::PLAYING;
        }
    }

    return state;
}

static void audioPlaying()
{
    LOGDEB("alsaEater: playing\n");
    audio_stopped = false;
}

static void audioStopped()
{
    LOGDEB("alsaEater: stopped\n");
    audio_stopped = true;
}

AudioEater alsaAudioEater(
    AudioEater::BO_HOST, &audioEater, &audioState, &audioPlaying, &audioStopped);

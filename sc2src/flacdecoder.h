/* Copyright (C) 2015-2018 J.F.Dockes
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef _FLACDECODER_H_INCLUDED_
#define _FLACDECODER_H_INCLUDED_

#include <FLAC/stream_decoder.h>

#ifdef WITH_OHBUILD
#include "OhmReceiver.h"
#else
#include <OpenHome/OhmReceiver.h>
#endif

#include "audiodecoder.h"

using namespace OpenHome;
using namespace OpenHome::Av;

class FlacDecoder : public AudioDecoder {
    static const int SAMPLES_BUF_SIZE = 16 * 1024;
public:
    FlacDecoder();
    ~FlacDecoder();

    void start();
    int decode(OhmMsgAudio& aMsg);
    void finish();
private:
    void putAudioMessage(
            unsigned int bits_per_sample,
            unsigned int channels,
            unsigned int frames,
            unsigned int sample_rate,
            bool halt);
    static FLAC__StreamDecoderReadStatus readCallback(
            const FLAC__StreamDecoder *decoder,
            FLAC__byte buffer[],
            size_t *bytes,
            void *client_data);
    static FLAC__StreamDecoderWriteStatus writeCallback(
            const FLAC__StreamDecoder *decoder,
            const FLAC__Frame *frame,
            const FLAC__int32 * const buffer[],
            void *client_data);
    static void metadataCallback(
            const FLAC__StreamDecoder *decoder,
            const FLAC__StreamMetadata *metadata,
            void *client_data);
    static void errorCallback(
            const FLAC__StreamDecoder *decoder,
            FLAC__StreamDecoderErrorStatus status,
            void *client_data);

    FLAC__StreamDecoder*  m_decoder;

    const unsigned char *m_buffer;
    unsigned int m_bytes;
    bool m_halt;

    char m_pBuffer[SAMPLES_BUF_SIZE];
#ifdef DEBUG_DECODER
    int dumpfd;
#endif
};

#endif /* _FLACDECODER_H_INCLUDED_ */

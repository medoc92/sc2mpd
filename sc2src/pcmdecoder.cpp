/* Copyright (C) 2015-2018 J.F.Dockes
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "pcmdecoder.h"
#include "rcvqueue.h"
#include "log.h"

#include <string.h>

#ifdef DEBUG_DECODER
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#endif

using namespace std;

PcmDecoder::PcmDecoder(bool needswap) : m_needswap(needswap)
{
    LOGDEB("Create PCM decoder\n");

#ifdef DEBUG_DECODER
    dumpfd = open("/tmp/sc2dump.pcm", O_WRONLY|O_CREAT|O_TRUNC, 0666);
#endif
}

PcmDecoder::~PcmDecoder()
{
    LOGDEB("Delete PCM decoder" << endl << flush);

#ifdef DEBUG_DECODER
    if (dumpfd > 0)
        close(dumpfd);
#endif
}

void PcmDecoder::start()
{
    LOGDEB("Start PCM decoder\n");
}

int PcmDecoder::decode(OhmMsgAudio& aMsg)
{
    unsigned int bytes = 0;
    unsigned int allocbytes = 0;
    char *buf = NULL;

    if (aMsg.Audio().Bytes() == 0) {
        if (aMsg.Halt()) {
            LOGDEB("PcmDecoder::decode: empty message with halt flag set\n");
            goto put_audio_message;
        } else {
            LOGDEB("PcmDecoder::decode: ignoring empty message\n");
            return 0;
        }
    } else if (aMsg.Halt()) {
        LOGDEB("PcmDecoder::decode: halt flag set in message\n");
    }

    bytes = aMsg.Audio().Bytes();
    // We allocate a bit more space to avoir reallocations in the resampler
    allocbytes = bytes + 100;
    buf = (char *)malloc(allocbytes);
    if (buf == 0) {
        LOGERR("PcmDecoder::decode: can't allocate " <<
                allocbytes << " bytes\n");
        return -1;
    }

    if (m_needswap) {
        copyswap((unsigned char *)buf, aMsg.Audio().Ptr(), bytes, aMsg.BitDepth());
    } else {
        memcpy(buf, aMsg.Audio().Ptr(), bytes);
    }

#ifdef DEBUG_DECODER
    if (dumpfd > 0)
        write(dumpfd, buf, bytes);
#endif

put_audio_message:
    AudioMessage *ap = new
        AudioMessage(aMsg.BitDepth(), aMsg.Channels(), aMsg.Samples(),
                     aMsg.SampleRate(), aMsg.Halt(), buf, allocbytes);

    // There is nothing special we can do if put fails: no way to
    // return status. Should we just exit ?
    if (!audioqueue.put(ap, false)) {
        LOGERR("sc2mpd: queue dead: exiting\n");
        exit(1);
    }

    return bytes;
}

void PcmDecoder::finish()
{
    LOGDEB("Finish PCM decoder\n");
}

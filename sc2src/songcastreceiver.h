/* Copyright (C) 2018 J.F.Dockes
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifdef WITH_OHBUILD
#include "Debug.h"
#include "OhmReceiver.h"
#else
#include <OpenHome/OhmReceiver.h>
#include <OpenHome/OhSongcast/Debug.h>
#endif

#include "audiodecoder.h"
#include "chrono.h"
#include "rcvqueue.h"

using namespace OpenHome;
using namespace OpenHome::Av;

class SongcastReceiver : public IOhmReceiverDriver, public IOhmMsgProcessor {
public:
   SongcastReceiver(AudioEater* eater, AudioEater::Context *ctxt);
   ~SongcastReceiver();

   virtual TBool IsConnected();
private:
   // IOhmReceiverDriver
   virtual void Add(OhmMsg& aMsg);
   virtual void Timestamp(OhmMsg& aMsg);
   virtual void Started();
   virtual void Connected();
   virtual void Playing();
   virtual void Disconnected();
   virtual void Stopped();

   // IOhmMsgProcessor
   virtual void Process(OhmMsgAudio& aMsg);
   virtual void Process(OhmMsgTrack& aMsg);
   virtual void Process(OhmMsgMetatext& aMsg);

private:
   // Debug, stats, etc while we get to understand the Songcast streams
   class Observer {
   public:
       TBool iReset;
       TUint iCount;
       TUint iFrame;
       int dumpfd;
       Chrono chron;
       Observer() : iReset(true), iCount(0), iFrame(0), dumpfd(-1) {
#if 0
           dumpfd = 
               open("/y/av/tmp/sc2dump", O_WRONLY|O_CREAT|O_TRUNC, 0666);
           if (dumpfd < 0) {
               LOGERR("SongcastReceiver::Open dump file failed\n");
           }
#endif
       }

       void reset() {
           iReset = true;
       }

       void process(OhmMsgAudio& aMsg);
   };
    Observer m_obs;
    AudioDecoder *m_decoder{nullptr};
    AudioEater *m_eater;
    TBool iConnected;
};

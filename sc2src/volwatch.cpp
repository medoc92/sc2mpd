/* Copyright (C) 2020 J.F.Dockes
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "config.h"

#include "volwatch.h"

#include <unistd.h>
#include <thread>
#include <math.h>
#include <mpd/client.h>

#include "rcvqueue.h"
#include "smallut.h"
#include "log.h"


using namespace std;
static thread o_worker;

static string o_mpdhost{"localhost"};
static int o_mpdport{6600};
static string o_mpdpassword;

static void watcher(void *)
{
    struct mpd_connection *connection =
        mpd_connection_new(o_mpdhost.c_str(), o_mpdport, 5000);
    if (nullptr == connection) {
        LOGERR("Volume watcher: can't open connection to mpd host " <<
               o_mpdhost << " port " << o_mpdport << endl);
        return;
    }

    for (;;) {
        mpd_status *mpds =  mpd_run_status(connection);
        if (nullptr != mpds) {
            int volume = mpd_status_get_volume(mpds);
            if (volume > 100)
                volume = 100;
            if (volume < 0)
                volume = 0;
            g_volumescale = (exp(volume / 25.0) - 1) / (exp(4.0) - 1);
            LOGDEB("volwatcher: new MPD volume: " << volume << "->" <<
                   g_volumescale << "\n");
        }
        enum mpd_idle idle = MPD_IDLE_MIXER;
        mpd_run_idle_mask(connection, idle);
    }
}

void startVolWatcher(ConfSimple& config)
{
    config.get("mpdhost", o_mpdhost);
    o_mpdport =  config.getInt("mpdport", 6600);
    config.get("mpdpassword", o_mpdpassword);
    bool followvolume = config.getBool("scusempdvolume", false);
    if (followvolume) {
        o_worker = thread(watcher, nullptr);
        o_worker.detach();
    }
}

/* Copyright (C) 2017 J.F.Dockes
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef _WATCHER_H_INCLUDED_
#define _WATCHER_H_INCLUDED_

#include <string>

#include "conftree.h"

/** 
  Start the watcher thread. If configured, it polls at regular
  intervals for audio activity (indicated by the queue packet counter,
  and, depending on configuration parameters, executes the external
  command with an argument of 'off' if the queue was idle for more
  than idlesecs seconds, and again with an argument of 'on' as soon as
  it detects new activity and/or closes/opens the alsa device */
extern void startWatcher(ConfSimple& config);

#endif /* _WATCHER_H_INCLUDED_ */
